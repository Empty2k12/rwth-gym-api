FROM golang:1.20.4-alpine3.17 as build

RUN apk update && apk add tesseract-ocr tesseract-ocr-dev build-base ca-certificates && rm -rf /var/cache/apk/*

WORKDIR /go/src/app

COPY . .

RUN go mod download

RUN go build -o app

FROM alpine:3.17

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN apk add tesseract-ocr tesseract-ocr-dev

COPY --from=build /go/src/app/app /usr/local/bin/app

ENTRYPOINT ["/usr/local/bin/app"]
