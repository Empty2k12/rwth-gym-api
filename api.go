package main

import (
    "fmt"
    "log"
	"net/http"
	"io"
	"os"
	"github.com/otiai10/gosseract/v2"
	"strconv"
	"encoding/json"
	"math"
	"time"
)

type Pumpers struct {
    Pumpers      int64 `json:"pumpers"`
	NextUpdate   time.Time `json:"next_update"`
}

var t = time.Now()
var int_pumpers int64 = 0

func handler(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	if(now.After(t)) {
		t = time.Now().Add(30 * time.Second)

		req, err := http.NewRequest("GET", "https://buchung.hsz.rwth-aachen.de/cgi/studio.cgi?size=30", nil)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("Accept", "image/webp,*/*")
		req.Header.Set("Referer", "https://buchung.hsz.rwth-aachen.de/angebote/aktueller_zeitraum/_Auslastung.html")
		response, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()

		file, err := os.Create("pumpers.png")
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		_, err = io.Copy(file, response.Body)
		if err != nil {
			log.Fatal(err)
		}
		
		client := gosseract.NewClient()
		client.SetPageSegMode(gosseract.PSM_SINGLE_WORD)
		defer client.Close()
		client.SetImage("pumpers.png")
		text, _ := client.Text()
		
		if s, err := strconv.ParseFloat(text, 64); err == nil {
			int_pumpers = int64(math.Round(s))
		}
	}

	if(r.URL.Path[1:] != "metrics") {
		pumpers := Pumpers{Pumpers: int_pumpers, NextUpdate: t}

		js, err := json.Marshal(pumpers)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	} else {
		fmt.Fprintf(w, `# HELP rwth_gym_pumpers Number of pumpers currently in the RWTH gym.
# TYPE rwth_gym_pumpers gauge
rwth_gym_pumpers %d`, int_pumpers)
	}
}

func main() {
    http.HandleFunc("/", handler)
    log.Fatal(http.ListenAndServe(":8080", nil))
}